import React, {
  Component
} from 'react';
import './App.css';
import Person from './Person/person';
class App extends Component {
  state = {
    person: [
      {
        name: 'gayatri', age: 24,
      },
      {name: 'amol', age: 29},
     { name: 'jitesh', age: 25},
    ],
    otherProp: 'this is extra property'
    }

    onClickHandler = ()=>{
      this.setState(
        {
          person: [
            {
              name: 'lalita', age: 45,
            },
            {name: 'amol', age: 29},
            { name: 'jitesh', age: 25}
          ]
        }
      )
    }
  render() {
    return ( 
    <div className = "App" >
      <h1>this is my first program </h1>
      <button className = "test" onClick= {this.onClickHandler}>click me to update data dynamic</button>
      <Person  name = {this.state.person[0].name} age = {this.state.person[0].age}/>
      <Person name = {this.state.person[1].name}  age =  {this.state.person[1].age}/>
      <Person name = {this.state.person[2].name} age =  {this.state.person[2].age} >
        It is working here also</Person>
      <Person />
      
    </div>
    );
  }
}

export default App;