import React from 'react'
const Person = (props) => {
    return (<div>
        I 'm {props.name} and I'm {props.age} years old 
        <p> {props.children}</p>
        </div>
    )

}

export default Person;